<?php 
get_header();
if ( ! isset ( get_queried_object()->taxonomy ) )
{
    return;
}
$taxonomy = get_queried_object();
$name_tax = $taxonomy->name;
$id_tax = $taxonomy->term_id;
$all_terms = get_terms( array(
    'taxonomy' => 'danh-muc-san-pham',
    'hide_empty' => false,
) );
$mtr_posts_per_page = get_option('posts_per_page',true);
$theme_option = get_option('theme_option');
$mtr_banner_category_product = isset($theme_option['mtr_banner_category_product']) ? $theme_option['mtr_banner_category_product']['url'] : "";

?>
<section class="banner  main-section" style="background-image: url('<?php echo esc_url($mtr_banner_category_product);?>');">
    <div class="container">
        <div class="row">
            <div class="section-part text-center">
                <h3 class="text-left text-white font-size-30">
                    <?php echo esc_html($name_tax);?>
                </h3>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row flex-column-reverse flex-md-row margin-top-24">
        <div class="col-sm-4 col-12">
            <div class="sidebar-ccv">
                <div class="item-sidebar">
                    <div class="title-sb">
                        <h3 class="text-uppercase">
                            SẢN PHẨM
                        </h3>
                        
                    </div>
                </div>
                <div class="content-sidebar">
                    <ul class="category">
                        <?php
                            foreach ($all_terms as $key => $term) { 
                                if($term->term_id == $id_tax ){
                                    $class= "class='active'";
                                } else {
                                    $class= "";
                                }
                                ?>
                                <li <?php echo $class;?>>
                                    <a href="<?php echo get_term_link($term->term_id);?>">
                                        <?php echo $term->name;?>
                                    </a>
                                </li>
                            <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <?php echo mtr_box_contact();?>
            <?php dynamic_sidebar( 'ccv-product-sidebar' ) ?>
        </div>
        <div class="col-sm-8 col-12">
            <div class="title-category">
                <h3><?php echo esc_html($name_tax);?></h3>
                <div class="list-tiem">
                    <div class="row">
                        <?php
                            if ( have_posts() ) {
                                while ( have_posts() ) :
                                    the_post();
                                    echo mtr_load_template( 'product/content', '', ['posts_per_page' => $mtr_posts_per_page]);
                                endwhile;
                            }
                            wp_reset_postdata();
                        
                        ?>
                        <?php echo tth_pagination();?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</section>
<?php

get_footer();?>