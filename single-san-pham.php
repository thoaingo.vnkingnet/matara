<?php 
get_header();
if(have_posts()) : the_post();
$url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
$theme_option = get_option('theme_option');
$mtr_banner_product = isset($theme_option['mtr_banner_product']) ? $theme_option['mtr_banner_product']['url'] : "";
$term = wp_get_post_terms(get_the_ID(),'danh-muc-san-pham')[0];
$id_term = $term->term_id;
$name_term = $term->name;
$all_terms = get_terms( array(
    'taxonomy' => 'danh-muc-san-pham',
    'hide_empty' => false,
) );
?>
<section class="banner  main-section" style="background-image: url('<?php echo esc_url($mtr_banner_product);?>');">
    <div class="container">
        <div class="row">
            <div class="section-part text-center">
                <h3 class="text-left text-white font-size-30">
                    <?php the_title();?>
                </h3>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row flex-column-reverse flex-md-row margin-top-24">
        <div class="col-sm-4 col-12">
            <div class="sidebar-ccv">
                <div class="item-sidebar">
                    <div class="title-sb">
                        <h3>
                            SẢN PHẨM
                        </h3>
                        
                    </div>
                </div>
                <div class="content-sidebar">
                    <ul class="category">
                        <?php
                            foreach ($all_terms as $key => $term) { 
                                if($term->term_id == $id_term ){
                                    $class= "class='active'";
                                } else {
                                    $class= "";
                                }
                                ?>
                                <li <?php echo $class;?>>
                                    <a href="<?php echo get_term_link($term->term_id);?>">
                                        <?php echo $term->name;?>
                                    </a>
                                </li>
                            <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <?php echo mtr_box_contact();?>
            <?php dynamic_sidebar( 'ccv-product-sidebar' ) ?>
        </div>
        <div class="col-sm-8 col-12">
            <div class="title-category single-product">
                <h3><?php the_title();?></h3>
            </div>
            <div class="content-ccv">
                <div class="row">
                    <div class="col-12">
                        <div class="single-content-ccv">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-relation">
                <h3 class="text-uppercase">
                    SẢN PHẨM <?php echo $name_term;?> KHÁC
                </h3>
                <?php echo new_product_relative_other(get_the_ID(),$id_term);?>
                
            </div>
        </div>
    </div>
    
</section>
<?php
endif;
get_footer();?>