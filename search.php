<?php
    get_header();
    $theme_option = get_option('theme_option');
    $mtr_banner = isset($theme_option['mtr_banner_blog']) ? $theme_option['mtr_banner_blog']['url'] : get_template_directory_uri()."/assets/img/banner-about.png";
    $mtr_posts_per_page = get_option('posts_per_page',true);
    ?>
    <section class="banner">
        <img class="img-fluid" src="<?php echo esc_url($mtr_banner);?>" alt="Tìm kiếm">
    </section>
    <section class="container">
        <div class="row flex-column-reverse flex-md-row margin-top-24">
            <div class="col-12">
                <h3 class="text-center text-uppercase color-primary">Kết quả tìm kiếm</h3>
            </div>
            
        </div>
        <div class="row list-tiem margin-top-24">
            <?php
            if ( have_posts() ) {
                while ( have_posts() ) :
                    the_post();
                    echo mtr_load_template( 'blog/content', '', ['posts_per_page' => $mtr_posts_per_page]);
                endwhile;
            }
            wp_reset_postdata();
            ?>
            
        </div>
        <?php echo tth_pagination();?>
        <!-- <div class="ccv_pagination row">
            <ul class="col-12 text-center">
                
                <li class="d-inline-block no-border">
                    <a href=""><i class="fas fa-backward"></i></a>
                </li>
                <li class="d-inline-block no-border"><a href=""><i class='fas fa-caret-left font-28'></i></a></li>
                <li class="d-inline-block"><a href="">1</a></li>
                <li class="d-inline-block"><a href="">2</a></li>
                <li class="d-inline-block"><a href="">3</a></li>
                <li class="d-inline-block no-border"><a href=""><i class='fas fa-caret-right font-28'></i></a></li>
                <li class="d-inline-block no-border ">
                    <a href=""><i class="fas fa-forward"></i></a>
                </li>
            </ul>
        </div> -->
    </section>
<?php get_footer();
?>