<?php 
get_header();
if(have_posts()) : the_post();
$url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
?>
<section class="banner  main-section" style="background-image: url('<?php echo esc_url($url_image_large);?>');">
    <div class="container">
        <div class="row">
            <div class="section-part text-center">
                <h3 class="text-left text-white font-size-30">
                    <?php the_title();?>
                </h3>
            </div>
        </div>
    </div>
</section>
<section class="container full-width margin-top-40">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center color-primary font-size-30">
            <?php the_title();?>
            </h3>
        </div>
    </div>
</section>
<section class="content-page container full-width margin-top-40">
    <?php the_content();?>
</section>
<?php
endif;
get_footer();?>