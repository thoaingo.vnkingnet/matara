jQuery(document).ready(function($) {
 
	$("a.search-ccv").click(function(e) {
        e.preventDefault();
        console.log("sdsdsds");
	   $(".togglesearch").toggle();
	   $(".togglesearch input[type='text']").focus();
	 });
    //WOW
    wow = new WOW(
      {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
    )
    wow.init();
});
jQuery(document).ready(function($) {
    var owl = $('.full-width.slider .owl-carousel');
    owl.owlCarousel({
      margin: 10,
      nav: false,
      loop: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    })
  });
  jQuery(document).ready(function($) {
    var owl = $('.full-width.partner .owl-carousel');
    owl.owlCarousel({
      margin: 18,
      nav: true,
      loop: true,
      dots:false,
      navText: [
        "<i class='fas fa-caret-left'></i>",
        "<i class='fas fa-caret-right'></i>"
     ],
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 4
        }
      }
    })
  });
  //Poup gallary
  jQuery(document).ready(function($) {
    $('.popup-gallery').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      zoom: {
        enabled: true, // By default it's false, so don't forget to enable it
    
        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        
      }
    });
  });

    //Contact send mail
jQuery(function($){
  var requestRunning = false;
  var xhr;
  var hasSentmail = false;
  jQuery(".button-ajax-sent-mail").click(function(event) {
      if (requestRunning) {
          xhr.abort();
      }
      hasSentmail = true;
      var data = [];
      event.preventDefault();
      var $form = $(this).closest(".tth-contact-form form");
      $form.find(".tth-loader-new").show();


      xhr = $.ajax({
          url: matara_params.ajax_url,
          dataType: 'json',
          type: 'GET',
          data: {
              action : 'tth_sent_mail_contact',
              fullname : $form.find('#fullname').val(),
              subtitle : $form.find('#subtitle').val(),
              phone : $form.find('#phone').val(),
              mail : $form.find('#mail').val(),
              content : $form.find('#content').val(),
              address : $form.find('#address').val(),
              recaptcha : $form.find('#g-recaptcha-response').val(),
          },
          success: function (res) {
              
          },
          complete: function (xhr) {
              $form.find(".tth-loader-new").hide();
              if(xhr.responseJSON.status != 0){
                  var mess = '<div class="ccv-success"><div class="content-message">'+xhr.responseJSON.message+'</div></div>';
                  
                  $form.find('#fullname').val("");
                  $form.find('#subtitle').val("");
                  $form.find('#phone').val("");
                  $form.find('#mail').val("");
                  $form.find('#content').val("");
                  $form.find('#address').val("");
                  $form.find(".tth-message-res").html(mess);
              } else{
                  var mess = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+xhr.responseJSON.message+'</div>';
                  $form.find(".tth-message-res").html(mess);
                  
                  grecaptcha.reset();
              }
              requestRunning = false;
          },
          error: function(err) {
              alert("Error sent mail");
              $form.find(".tth-loader-new").hide();
          }
      });

      requestRunning = true;
  });
});