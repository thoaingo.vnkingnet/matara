<?php
    $theme_option = get_option('theme_option');
    $matara_slider_partner = isset($theme_option['matara_slider_partner']) ? $theme_option['matara_slider_partner'] : array();
    $matara_partner_title = isset($theme_option['matara_partner_title']) ? $theme_option['matara_partner_title'] : "";
    $matara_partner_content = isset($theme_option['matara_partner_content']) ? $theme_option['matara_partner_content'] : "";
    $matara_link_facebook = isset($theme_option['matara_link_facebook']) ? $theme_option['matara_link_facebook'] : "";
    $matara_link_youtube = isset($theme_option['matara_link_youtube']) ? $theme_option['matara_link_youtube'] : "";
    $matara_link_twitter = isset($theme_option['matara_link_twitter']) ? $theme_option['matara_link_twitter'] : "";
    $matara_main_footer = isset($theme_option['matara_main_footer']) ? $theme_option['matara_main_footer'] : "";
    $matara_copyright = isset($theme_option['matara_copyright']) ? $theme_option['matara_copyright'] : "";
?>
            <section class="full-width back-bg-green partner margin-top-40">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="text-center color-primary">
                                <?php echo esc_html($matara_partner_title);?>
                            </h3>
                            <div class="content-about">
                                <?php echo htmlspecialchars_decode($matara_partner_content);?>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if(!empty($matara_slider_partner)){ ?>
                            <div class="row">
                                <div class="owl-carousel owl-theme">
                                    <?php
                                        foreach ($matara_slider_partner as $key => $logo_partner) {
                                    ?>
                                    <div class="item">
                                        <img class="d-block img-fluid" src="<?php echo esc_url($logo_partner['image']);?>" alt="<?php echo esc_attr($logo_partner['title']);?>">
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        <?php }
                    ?>
                </div>
            </section>
		</main>
		<footer>
			<div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-sm-flex align-items-end justify-content-between">
                            <ul class="social">
                                <li class="facebook">
                                    <a href="<?php echo esc_url($matara_link_facebook);?>"><i class="fab fa-facebook"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="<?php echo esc_url($matara_link_youtube);?>"><i class="fab fa-youtube"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="<?php echo esc_url($matara_link_twitter);?>"><i class="fab fa-twitter"></i></a>
                                </li>
                            </ul>
                            <div class="menu-footer main-menu">
                            <?php
									$primarymenu = array(
										'theme_location'  => 'footer',
										'menu'            => '',
										'container'       => '',
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'd-sm-flex justify-content-end',
										'menu_id'         => '',
										'echo'            => true,
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 0,
										'walker' => new Walker_Nav_Menu_Custom()
										);
										if ( has_nav_menu( 'footer' ) ) {
										wp_nav_menu( $primarymenu );
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-footer text-center">
                <?php echo htmlspecialchars_decode($matara_main_footer);?>
            </div>
            <div class="bottom-footer">
                <p class="text-center"><?php echo esc_html($matara_copyright);?></p>
            </div>
        </footer>
        <?php wp_footer();?>
        <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=275241790088186&autoLogAppEvents=1"></script>
    </body>
</html>