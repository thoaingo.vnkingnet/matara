<?php 
get_header();
?>
<?php $theme_option = get_option('theme_option');
$banner_new = isset($theme_option['banner_new']) ? $theme_option['banner_new']['url'] : "";

if(have_posts()): the_post();
$term = wp_get_post_terms(get_the_ID(),'category')[0];
$id_term = $term->term_id;
$name_term = $term->name;
$get_post_time = get_the_time('', get_the_ID());
$get_the_date = get_the_date('d/m/Y', get_the_ID());
    if(!empty($banner_new)){ ?>
        <section class="banner">
            <img class="img-fluid" src="<?php echo esc_url($banner_new);?>" alt="">
        </section>
    <?php }
?>

<section class="container single-post">
    <div class="row flex-column-reverse flex-md-row margin-top-24">
        <div class="col-12">
            <h3 class="title text-left text-uppercase color-primary border-dots">
                <?php the_title();?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 col-12">
            <div class="date-post">
                <em><?php echo $get_post_time;?>, <?php echo esc_html($get_the_date);?></em>
            </div>
            <div class="content-single-post">
                <?php the_content();?>
            </div>
            <div class="product-relation">
                <h3>
                    TIN TỨC KHÁC
                </h3>
                <?php echo new_post_relative_other(get_the_ID(),$id_term);?>
            </div>
        </div>
        <div class="col-sm-4 col-12">
            <div class="sidebar-ccv">
                <?php dynamic_sidebar( 'blog-sidebar' ) ?>
            </div>
            
        </div>
    </div>
</section>
<?php
endif;
get_footer();?>