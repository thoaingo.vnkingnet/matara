<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $theme_option = get_option('theme_option');
        $mtr_logo = isset($theme_option['mtr_logo']) ? $theme_option['mtr_logo']['url'] : get_template_directory_uri()."/assets/img/logo.png";
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head();?>
        <script>
	// Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js     = d.createElement(s);
            js.id  = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        window.fbAsyncInit = function () {
            FB.init({
                appId  : '603999353503582',
                cookie : true,  // enable cookies to allow the server to access
                                // the session
                xfbml  : true,  // parse social plugins on this page
                version: 'v3.1' // use graph api version 2.8
            });

        };
    </script>
    <div class="fb-messengermessageus" 
    messenger_app_id="<APP_ID>" 
    color="<blue | white>"
    size="<standard | large | xlarge>">
    </div>
    </head>
    <body>
        <header class="d-flex justify-content-between">
            <div class="container">
                <div class="row ">
                    <div class="main-menu col-12">
                        <nav class="navbar navbar-expand-lg navbar-light bg-white align-items-end">
                            <a class="navbar-brand ccv-logo" href="<?php echo get_home_url()?>"><img class="img-fluid" src="<?php echo esc_url($mtr_logo);?>" alt=""></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="translate-ccv">
                                <div class="flag-item align-items-end">
                                <?php echo do_shortcode('[gtranslate]'); ?>
                                </div>
                                
                                <div class="collapse navbar-collapse" id="navbarText">
                                    <?php
                                        $primarymenu = array(
                                            'theme_location'  => 'primary',
                                            'menu'            => '',
                                            'container'       => '',
                                            'container_class' => '',
                                            'container_id'    => '',
                                            'menu_class'      => 'navbar-nav mr-auto justify-content-end',
                                            'menu_id'         => '',
                                            'echo'            => true,
                                            'before'          => '',
                                            'after'           => '',
                                            'link_before'     => '',
                                            'link_after'      => '',
                                            'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                                            'depth'           => 0,
                                            'walker' => new Walker_Nav_Menu_Custom()
                                            );
                                            if ( has_nav_menu( 'primary' ) ) {
                                            wp_nav_menu( $primarymenu );
                                        }
                                    ?>

                                    <span class="navbar-text">
                                        <a href="" class="search-ccv"><i class="fas fa-search"></i></a>
                                        <form class="togglesearch" method="GET">
                                            <div class="d-flex justify-content-between">
                                                <div class="input-group-prepend">
                                                    <input type="text" class="form-control" name="s" placeholder=""/>
                                                </div>
                                                
                                                <input type="submit" class="form-control" value="Tìm kiếm"/>
                                            </div>
                                            
                                        </form>
                                    </span>
                                </div>
                            </div>
                            
                            
                          </nav>
                    </div>
                </div>
            </div>
			
        </header>
        <main>
