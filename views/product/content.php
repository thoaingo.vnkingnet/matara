<div class="col-sm-6 col-12">
    <div class="item">
        <?php if ( has_post_thumbnail() ) {
            $url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
        ?>
        <div class="image">
            <a href="<?php the_permalink();?>">
                <img class="img-fluid"  src="<?php echo esc_url($url_image_large);?>" alt="<?php the_title();?>">
            </a>
        </div>
        <?php }?>
        <div class="title-item">
            <h4>
                <a href="<?php the_permalink();?>" class="color-primary text-uppercase"> <?php echo tth_title_excerpt(8)?></a>
            </h4>
        </div>
        <div class="desc">
            <p>
            <?php echo tth_content_excerpt(40)?>
            </p>
        </div>
    </div>
</div>