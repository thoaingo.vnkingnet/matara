<?php

$class = "item-post ";
if ( isset( $cols ) ) {
	if ( $cols == 3 ) {
		$class = "item-post";
	}
}
?>
<div class="col-sm-4 col-12">
    <div class="item">
        <div class="date text-center">
            <div class="day_ccc">
                <?php echo get_the_date('d', get_the_ID());?>
            </div>
            <div class="month-year">
            <?php echo get_the_date('m/Y', get_the_ID());?>
            </div>
        </div>
        <?php if ( has_post_thumbnail() ) {
            $url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
        ?>
        <div class="image">
            <a href="<?php the_permalink();?>">
                <img class="img-fluid" src="<?php echo esc_url($url_image_large);?>" alt="<?php the_title();?>">
            </a>
        </div>
        <?php }?>
        <div class="title-item">
            <h4>
                <a href="<?php the_permalink();?>" class="color-primary"><?php echo tth_title_excerpt(8)?></a>
            </h4>
        </div>
        <div class="desc">
            <p class="font-18">
                <?php echo tth_content_excerpt(50)?>
            </p>
        </div>
    </div>
</div>
