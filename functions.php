<?php 
if ( !class_exists( 'Redux' ) && file_exists( get_template_directory() . '/includes/plugin/redux-framework/ReduxCore/framework.php' ) ) {
    require get_template_directory()  . '/includes/plugin/redux-framework/ReduxCore/framework.php' ;
}
if ( !isset( $redux_demo ) && file_exists( get_template_directory()  . '/includes/plugin/redux-framework/sample/sample-config.php' ) ) {
    require get_template_directory()  . '/includes/plugin/redux-framework/sample/sample-config.php' ;
}
require get_template_directory()  . '/includes/custom-walker-nav-menu.php' ;
require get_template_directory()  . '/includes/register-posttype.php' ;
require get_template_directory()  . '/includes/widget/RecentNews.php' ;
function ccv_theme_setup() {
    add_theme_support( 'custom-header' ); 
    add_theme_support( 'custom-background' );
    add_theme_support ('title-tag');
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', array(
        'video',
        'gallery',
        'image'
    ) );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list','gallery', ) );
    add_post_type_support( 'golf', array( 'comments' ) );
    register_nav_menus( array(
        'primary' => esc_html__('Primary Navigation Menu (Use For All Page)','matara'),   
        
    ) );
    register_nav_menus( array(
        'footer' => esc_html__('Menu Footer','matara'),   
        
    ) );
    register_nav_menus( array(
        'top' => esc_html__('Menu Top','matara'),   
        
    ) );
}
add_action( 'after_setup_theme', 'ccv_theme_setup' );
function matara_load_theme_textdomain() {
    load_theme_textdomain( 'matara', get_template_directory_uri() . '/languages/' );
}
add_action( 'after_setup_theme', 'matara_load_theme_textdomain' );
function matara_theme_scripts_styles(){
    /**** Theme Specific CSS ****/
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'family-fonts-Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat', array(), '1.0.0' );
    wp_enqueue_style( 'family-fonts-Roboto', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap' );
    wp_enqueue_style( 'owl.carousel.css', get_template_directory_uri().'/assets/css/owl.carousel.min.css', array(), true,false );
    wp_enqueue_style( 'owl.theme.default.css', get_template_directory_uri().'/assets/css/owl.theme.default.min.css', array(), true,false );
    wp_enqueue_style( 'magnific-popup.css', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css', array(), true,false );
    wp_enqueue_style( 'bootstrap.css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array(), true,false );
    wp_enqueue_style( 'custom_style.css', get_template_directory_uri().'/assets/css/style.css', array(), true,false );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    /**** Start Jquery ****/ 
    wp_enqueue_script("fontawesome.min", "https://kit.fontawesome.com/35aeac332b.js",array(),true,false);
    wp_enqueue_script("recaptcha.js", "https://www.google.com/recaptcha/api.js?ver=1",array(),true,false);
    wp_enqueue_script("wow.min", "https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js",array(),false,true);
    wp_enqueue_script("magnific-popup.", "https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js",array(),false,true);
    wp_enqueue_script("bootstrap-js.", "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",array(),false,true);
    wp_enqueue_script("owl.carousel.js", get_template_directory_uri()."/assets/js/owl.carousel.js",array(),false,true);
    wp_enqueue_script("main.js", get_template_directory_uri()."/assets/js/main.js",array(),false,true);
    wp_localize_script('jquery', 'matara_params', [
        'theme_url' => get_template_directory_uri(),
        'site_url' => site_url(),
        'ajax_url' => admin_url('admin-ajax.php'),
    ]);
}
add_action( 'wp_enqueue_scripts', 'matara_theme_scripts_styles' );

function ccv_add_sidebar() {
    register_sidebar([
        'name' => __('Blog Sidebar', ST_TEXTDOMAIN),
        'id' => 'blog-sidebar',
        'description' => __('Widgets in this area will be shown on shop pages.', ST_TEXTDOMAIN),
        'before_title' => '<h3 class="title text-left text-uppercase color-primary>',
        'after_title' => '</h3>',
        'before_widget' => '<div id="%1$s" class="item-sidebar %2$s">',
        'after_widget' => '</div>',
    ]);
}
function mtr_load_template($slug, $name = false, $data = array())
    {
        $template_dir = 'views';
        if (is_array($data))
            extract($data);

        if ($name) {
            $slug = $slug . '-' . $name;
        }
        $template = locate_template($template_dir . '/' . $slug . '.php');
        if (is_file($template)) {
            ob_start();
            include $template;
            $data = @ob_get_clean();

            return $data;
        }
}

if (!function_exists('matara_add_sidebar')) {

    function matara_add_sidebar() {
        register_sidebar([
            'name' => __('Blog Sidebar', 'matara'),
            'id' => 'blog-sidebar',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'matara'),
            'before_title' => '<h4>',
            'after_title' => '</h4>',
            'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
            'after_widget' => '</div>',
        ]);

        register_sidebar([
            'name' => __('Product Sidebar', 'matara'),
            'id' => 'ccv-product-sidebar',
            'description' => __('Widgets in this area will be shown on all product.', 'matara'),
            'before_title' => '<div class="item-sidebar"><div class="title-sb"><h3 class="text-uppercase">',
            'after_title' => '</h3></div></div>',
            'before_widget' => '<div id="%1$s" class="sidebar-ccv %2$s">',
            'after_widget' => '</div>',
        ]);
    }
}
add_action( 'widgets_init', 'matara_add_sidebar' );
function matara_select_posttype($post_type){
    $term = isset($_POST['query']) ? esc_attr($_POST['query']) : "";
    $get_post = get_posts( array( 'post_type'=> $post_type, 'posts_per_page' =>-1, 'post_status' => 'publish' ) );
    $sliders = [];
    if(!empty($get_post)){
        foreach ($get_post as $k => $v){
            $sliders[$v->ID] = $v->post_title;
        }
    }
    return $sliders;
}
function matara_get_slider(){ 
    $theme_option = get_option('theme_option');
    $mtr_sliders = isset($theme_option['matara_slider']) ? $theme_option['matara_slider'] : "";
    $get_sliders = get_posts( array( 'post_type'=> 'cv_slider', 'posts_per_page' =>-1, 'post_status' => 'publish', 'post__in' =>($mtr_sliders),'orderby'=> 'post__in' ) );
    if(!empty($get_sliders)){ 
        ?>
        <section class="container-fluid full-width slider">
            <div class="row">
                <div class="owl-carousel owl-theme">
                <?php foreach ($get_sliders as $k => $sl){
                    $url_image = wp_get_attachment_url( get_post_thumbnail_id($sl->ID,'full') );
                    ?>
                    <div class="item">
                        <img class="d-block img-fluid" title="<?php echo esc_attr($sl->post_title);?>" src="<?php echo esc_url($url_image);?>" alt="">
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    
    <?php }
}
function matara_get_about_home(){
    $theme_option = get_option('theme_option');
    $matara_about_title = isset($theme_option['matara_about_title']) ? $theme_option['matara_about_title'] : "GIỚI THIỆU <strong>MATARA PHARMA</strong>";
    $matara_about_content = isset($theme_option['matara_about_content']) ? $theme_option['matara_about_content'] : "<p>
                        Giới thiêu về công ty và triết lý kinh doanh. Ví dụ Matara Pharma là đơn vị nghiên cứu, phát triển song song với chuyển giao kỹ thuật, cung ứng nguyên liệu các loại dược mỹ phẩm chất lượng cao, chủng loại sản phẩm đa dạng phong phú, kể cả những sản phẩm chuyên biệt kháng corticoid. Với đội ngũ chuyên viên, nhân viên có kinh nghiệm và trình độ chuyên môn cao cùng dịch vụ chăm sóc khách hàng chuyên nghiệp, Matara Pharma sẽ mang đến sự hài lòng cho quý khách hàng.
                    </p>";
    $matara_about_page = isset($theme_option['matara_about_page']) ? $theme_option['matara_about_page'] : "";
    $matara_about_button = isset($theme_option['matara_about_button']) ? $theme_option['matara_about_button'] : "";
    if(!empty($matara_about_page)){
        $url_page = get_the_permalink($matara_about_page);
    } else {
        $url_page = "#";
    }
    ?>
    <section class="container full-width margin-top-40">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center color-primary">
                    <?php echo htmlspecialchars_decode($matara_about_title);?>
                </h3>
                <div class="content-about">
                    <?php echo htmlspecialchars_decode($matara_about_content);?>
                </div>
            </div>
            <div class="col-12">
                <div class="button-link text-center ">
                    <a href="<?php echo esc_url($url_page);?>" class="btn btn-primary btn-1"><?php echo esc_html($matara_about_button);?></a>
                </div>
                
            </div>
        </div>
    </section>
<?php }

function matara_product_feature_loop(){
    $theme_option = get_option('theme_option');
    $matara_product_feature_loop = isset($theme_option['matara_product_feature_loop']) ? $theme_option['matara_product_feature_loop'] : "";
    if(!empty($matara_product_feature_loop)){ ?>
        <section class="container full-width margin-top-40">
            <div class="row">
                <?php foreach ($matara_product_feature_loop as $key => $box) { ?>
                    <div class="col-sm-4 col-12 item-box">
                        <a class="item-box-link" href="<?php echo esc_url($box['url']);?>">
                            <img class="img-fluid" src="<?php echo esc_url($box['image']);?>" alt="<?php echo esc_attr($box['title']);?>">
                        </a>
                    </div>
                <?php }?>
                
                
            </div>
        </section>
    <?php }
}

function matara_product_feature(){ 
    $theme_option = get_option('theme_option');
    $matara_about_feature_title = isset($theme_option['matara_about_feature_title']) ? $theme_option['matara_about_feature_title'] : "";
    $matara_about_feature_content = isset($theme_option['matara_about_feature_content']) ? $theme_option['matara_about_feature_content'] : "";
    $matara_about_page_timhieu = isset($theme_option['matara_about_page_timhieu']) ? $theme_option['matara_about_page_timhieu'] : "";
    $matara_about_product_button = isset($theme_option['matara_about_product_button']) ? $theme_option['matara_about_product_button'] : "";
    $matara_product_feature_loop_new = isset($theme_option['matara_product_feature_loop_new']) ? $theme_option['matara_product_feature_loop_new'] : "";
    if(!empty($matara_about_page_timhieu)){
        $url_page = get_the_permalink($matara_about_page_timhieu);
    } else {
        $url_page = "#";
    }
    ?>
    <section class="container full-width margin-top-40">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center color-primary">
                        <?php echo htmlspecialchars_decode($matara_about_feature_title);?>
                    </h3>
                    <div class="content-about">
                        <?php echo htmlspecialchars_decode($matara_about_feature_content);?>
                    </div>
                </div>
                <?php if(!empty($matara_product_feature_loop_new)){ 
                foreach ($matara_product_feature_loop_new as $key => $cate) { ?>
                <div class="col-sm-4 col-12">
                    <div class="item-box-product <?php if($key==1){ echo 'bg-pink';} elseif($key==2){ echo 'bg-brown';} else {}?>">
                        <img class="img-fluid" src="<?php echo esc_url($cate['image']);?>" alt="<?php echo esc_attr($cate['title']);?>">
                        <div class="button-name">
                            <a href="<?php echo esc_url($cate['url']);?>" class="btn btn-primary text-white"><?php echo esc_attr($cate['title']);?></a>
                        </div>
                    </div>
                </div>
                <?php }
                } ?>
                <div class="col-12">
                    <div class="button-link text-center marrgin-top-36">
                        <a href="<?php echo esc_url($url_page);?>" class="btn btn-primary btn-1"><?php echo esc_html($matara_about_product_button);?></a>
                    </div>
                    
                </div>
            </div>
        </section>
<?php }
function matara_new_post_home(){
    $theme_option = get_option('theme_option');
    $matara_new_posts = isset($theme_option['matara_new_posts']) ? $theme_option['matara_new_posts'] : array();
    $matara_new_title_home = isset($theme_option['matara_new_title_home']) ? $theme_option['matara_new_title_home'] : "";
    //$get_posts = get_posts( array( 'post_type'=> 'post', 'posts_per_page' =>-1, 'post_status' => 'publish', 'post__in' => ($matara_new_posts),'orderby'=> 'post__in' ) ); 
    $get_posts = get_posts( array( 'post_type'=> 'post', 'posts_per_page' =>5, 'post_status' => 'publish','orderby'=> 'DESC' ) ); 
    ?>
    <section class="container full-width margin-top-40">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center color-primary">
                    <?php echo esc_html($matara_new_title_home);?>
                </h3>
                
            </div>
        </div>
        <?php if(!empty($get_posts)){
            $url_image_large = wp_get_attachment_url( get_post_thumbnail_id($get_posts[0]->ID,'full') );
            $title_large = $get_posts[0]->post_title;
            $title_des = $get_posts[0]->post_content;
            $words = 75;
            $more = ' … <i class="fas fa-forward"></i>';
            
            $excerpt_title = wp_trim_words( $title_des, $words, $more );

            ?>
            <div class="row  margin-top-40">
                <div class="col-sm-6 col-12">
                    <div class="img-blog img-blog-large">
                        <a href="<?php echo esc_url(get_the_permalink($get_posts[0]->ID ));?>">
                            <img class="img-fluid" src="<?php echo esc_url($url_image_large);?>"  alt="<?php echo esc_attr($title_large);?>">
                        </a>
                        
                    </div>
                    <div class="content-blog-infor">
                        <h2>
                            <?php echo esc_html($title_large);?>
                        </h2>
                        <p class="short-description">
                            <?php echo htmlspecialchars_decode($excerpt_title);?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="row">
                        <?php foreach ($get_posts as $key => $post_new) {
                            if($key != 0){
                                $url_image_new = wp_get_attachment_url( get_post_thumbnail_id($post_new->ID,'full') );
                                $title_new = $post_new->post_title;
                            ?>
                                <div class="col-sm-6 col-12 item-blog-small">
                                    <div class="img-blog img-blog-large">
                                        <a href="<?php echo esc_url(get_the_permalink($post_new->ID ));?>">
                                            <img class="img-fluid" src="<?php echo esc_url($url_image_new);?>"  alt="<?php echo esc_attr($title_new);?>">
                                        </a>
                                        
                                    </div>
                                    <div class="content-blog-infor style-small">
                                        <h3>
                                            <?php echo esc_html($title_new);?>
                                        </h3>
                                    </div>
                                </div>
                            <?php }
                        }?>
                    </div>
                    
                </div>
            </div>

        <?php } ?>
    </section>
    
 <?php  
}
function new_post_relative_other($id_post, $id_term){
    $get_posts = get_posts( array( 'post_type'=> 'post', 
    'posts_per_page' =>5, 
    'post_status' => 'publish', 
    'post__not_in' => array($id_post),
    'orderby'=> 'post__not_in',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => array($id_term),
        ),
    ),
    ) );
    if($get_posts){ ?>
    <ul>
        <?php
            foreach ($get_posts as $key => $recent) { 
                $words = 8;
                $more = ' …   ';
                
                $excerpt_title = wp_trim_words( get_the_title($recent->ID), $words, $more );
                ?>
                <li>
                    <a href="<?php echo get_the_permalink($recent->ID);?>"><i class='fas fa-caret-right'></i><?php echo $excerpt_title;?> <span> <?php echo  get_the_date('d/m/Y', $recent->ID);?></span></a>
                </li>
            <?php }

        ?>
       
       
    </ul>
<?php    }
}
function new_product_relative_other($id_post,$term){
    $get_posts = get_posts( array( 
        'post_type'=> 'san-pham',
        'posts_per_page' =>5, 
        'post_status' => 'publish', 
        'post__not_in' => array($id_post),
        'tax_query' => array(
            array(
                'taxonomy' => 'danh-muc-san-pham',
                'field'    => 'term_id',
                'terms'    => array($term),
            ),
        ),
        ) );
    if($get_posts){ ?>
    <ul>
        <?php
            foreach ($get_posts as $key => $recent) { 
                $words = 8;
                $more = ' …   ';
                
                $excerpt_title = wp_trim_words( get_the_title($recent->ID), $words, $more );
                ?>
                <li>
                    <a href="<?php echo get_the_permalink($recent->ID);?>"><i class='fas fa-caret-right'></i><?php echo $excerpt_title;?></a>
                </li>
            <?php }
            wp_reset_postdata();

        ?>
       
       
    </ul>
<?php    }
}

if ( ! function_exists( 'tth_esc_data' ) ) {
	function tth_esc_data( $var_esc, $type = '' ) {
		switch ( $type ) {
			case 'html':
				return esc_html( $var_esc );
				break;
			case 'attr':
				return esc_attr( $var_esc );
				break;
			case 'url':
				return esc_url( $var_esc );
				break;
			case 'tags':
				$balance = 'balance';
				$tags    = 'Tags';
				$string  = call_user_func_array( $balance . $tags, [ $var_esc ] );

				return $string;
				break;
			case '':
			default:
				return $var_esc;
				break;
		}
	}
}
function tth_content_excerpt($limit){
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'.';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}
function tth_title_excerpt($limit){
    $excerpt = explode(' ', get_the_title(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'.';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}
function tth_pagination() {
    if( is_singular() )
        return;
    global $wp_query;
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 4 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<nav aria-label="Pagination Archive" class="ccv_pagination row"><ul class="col-12 text-center">' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="d-inline-block font-28">%s</li>' . "\n", get_previous_posts_link("<i class='fas fa-caret-left font-28'></i>") );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="d-inline-block active"' : 'class="d-inline-block"';
 
        printf( '<li %s class="d-inline-block"><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 4, $links ) )
            echo '<li class="d-inline-block"><a class="">…</a></li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="d-inline-block active"' : ' class="d-inline-block"';
        printf( '<li %s><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 2, $links ) )
            echo '<li class="d-inline-block"><a class="" >…</a></li>' . "\n";
 
        $class = $paged == $max ? ' class="d-inline-block active"' : ' class="d-inline-block"';
        printf( '<li %s><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="d-inline-block font-28">%s</li>' . "\n", get_next_posts_link("<i class='fas fa-caret-right font-28'></i>") );
 
    echo '</ul></nav>' . "\n";
}
//Send mail
add_action( 'wp_ajax_tth_sent_mail_contact', 'tth_sent_mail_contact' );
add_action( 'wp_ajax_nopriv_tth_sent_mail_contact', 'tth_sent_mail_contact' );
function tth_sent_mail_contact(){
    $fullname = isset($_GET['fullname']) ? trim($_GET['fullname']) : '';
    $subtitle = isset($_GET['subtitle']) ? trim($_GET['subtitle']) : '';
    $mail = isset($_GET['mail']) ? $_GET['mail'] : '';
    $content = isset($_GET['content']) ? $_GET['content'] : '';
    $phone = isset($_GET['phone']) ? $_GET['phone'] : '';
    $address = isset($_GET['address']) ? $_GET['address'] : '';
    $recaptcha = isset($_GET['recaptcha']) ? $_GET['recaptcha'] : '';
    $status = 0;
    $message ='';
    $subject_tth = $fullname;
    if(empty($recaptcha)){
        echo json_encode(
        array(
            'status' => 0,
            'message' => esc_html__('Captcha is required','matara').'<br>',
            )
        );
        die();
    }
    if(empty($fullname) || empty($subtitle) || (empty($mail))  || (empty($content)) || (empty($phone)) || (empty($address))){
        $status = 0;
        if(empty($fullname)){
            $message .= esc_html__('Vui lòng nhập họ tên','matara').'<br>';
        }
        if(empty($subtitle)){
            $message .= esc_html__('Vui lòng nhập tiêu đề','matara').'<br>';
        }
        if(empty($mail)){
            $message .= esc_html__('Vui lòng nhập  mail','matara').'<br>';
        }
        if(empty($phone)){
            $message .= esc_html__('Vui lòng nhập số điện thoại','matara').'<br>';
        }
        if(empty($content)){
            $message .= esc_html__('Vui lòng nhập nội dung','matara').'<br>';
        }
        if(empty($address)){
            $message .= esc_html__('Vui lòng nhập địa chỉ','matara').'<br>';
        }
        echo json_encode(
            array(
                'status' => $status,
                'message' => $message,
                )
            );
    } else {
        $status = 1;
        $theme_option = get_option('theme_option');
        $admin_email = isset($theme_option['email_admin_address']) ? $theme_option['email_admin_address'] : "";
        $body_email = "<html><body><h2>".$subject_tth."</h2>";
                        "<strong>".__('Họ và tên','matara')."</strong>: ".$subject_tth."<br/>".
                        "<strong>".__('Email','matara')."</strong>: ".$mail."<br/>".
                        "<strong>".__('Số điện thoại','matara')."</strong>: ".$phone."<br/>".
                        "<strong>".__('Nội dung','matara')."</strong>: ".$content."<br/></html></body>";

        $multiple_to_recipients = array($admin_email);
        $subject = $subject_tth;
        $body    = $body_email;
        $headers = 'From:' . $subject . "<" . $mail . ">" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= 'Cc: '.$admin_email;
        $attachment = false;
        
        try {
            add_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
            $check = tth_send_mail($multiple_to_recipients, $subject, $body, $headers,$attachment);
            tth_rm_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
            if( $check){
                echo json_encode(
                    array(
                        'status' => $status,
                        'message' => esc_html__('Cám ơn bạn đã liên hệ với chúng tôi!<br>Chúng tôi sẽ liên hệ lại với bạn sớm nhất có thể.', 'matara'),
                        )
                );
            } else {
                echo json_encode(
                    array(
                        'status' => 0,
                        'message' => esc_html__('Lỗi gửi mail.', 'matara'),
                        )
                );
            }
        } catch (Exception $e) {
            echo json_encode(
                array(
                    'status' => 0,
                    'message' =>  $e->getMessage(),
                    )
            );
        }
        
        
        
    }
    die();
}
function set_html_content_type_sent_email()
{
    return 'text/html';
}
if(!function_exists('tth_send_mail')){
	function tth_send_mail($multiple_to_recipients, $subject, $body, $headers,$attachment){
		$check = wp_mail($multiple_to_recipients, $subject, $body, $headers,$attachment);
		return $check;
	}
}
if (!function_exists('tth_rm_filter')) {
	function tth_rm_filter($tag, $function_to_remove, $priority = 10)
	{
		return remove_filter( $tag, $function_to_remove, $priority );
	}
}
function mtr_box_contact(){
    $theme_option = get_option('theme_option');
    $mtr_contact_product = isset($theme_option['mtr_contact_product']) ? $theme_option['mtr_contact_product'] : "";
    $mtr_contact_text_product = isset($theme_option['mtr_contact_text_product']) ? $theme_option['mtr_contact_text_product'] : "";
    $mobile = str_replace(" ", "", $mtr_contact_product); ?>
    <div class="sidebar-ccv">
        <div class="item-sidebar">
            <div class="title-sb">
                <h3>
                    LIÊN HỆ VỚI MATARA
                </h3>
                
            </div>
        </div>
        <div class="content-sidebar">
            <ul>
                <li class="phone-contact">
                    <a href="tel:<?php echo esc_attr($mobile);?>"><span><i class="fas fa-phone-alt"></i></span><?php echo esc_html($mtr_contact_product);?></a>
                </li>
                <li class="active inline-text">
                    <em><?php echo esc_html($mtr_contact_text_product);?></em>
                </li>
            </ul>
        </div>
    </div>
<?php }
function bbit_che_do_bao_tri()
{
    if (!current_user_can('edit_themes') || !is_user_logged_in()) {
        wp_die('Trang web tạm thời đang được bảo trì. Xin vui lòng quay trở lại sau.');
    }
}
add_action('get_header', 'bbit_che_do_bao_tri');