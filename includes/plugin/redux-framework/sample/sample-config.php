<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "theme_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Matara Options', 'matara' ),
        'page_title'           => __( 'Matara Options', 'matara' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => 'redux_matara',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.


    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        //$args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'matara' ), $v );
    } else {
        //$args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'matara' );
    }

    // Add content after the form.
    //$args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'matara' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'matara' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'matara' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'matara' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'matara' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'matara' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Cài đặt chung', 'matara' ),
        'id'               => 'basic',
        'desc'             => __( 'Cài đặt chung!', 'matara' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Header', 'matara' ),
        'id'               => 'matara-header',
        'subsection'       => true,
        'customizer_width' => '450px',
        'desc'             => '',
        'fields'           => array(
            array(
                'id'         => 'mtr_logo',
                'type'       => 'media',
                'title'      => __( 'Upload logo', 'matara' ),
                'full_width' => true,
                'default' => array( 'url' => get_template_directory_uri() . '/assets/img/logo.png' ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer', 'matara' ),
        'id'               => 'matara-footer',
        'subsection'       => true,
        'customizer_width' => '450px',
        'desc'             => '',
        'fields'           => array(
            array(
                'id'       => 'matara_partner_title',
                'type'     => 'text',
                'title'    => __( 'Tiêu đề partner', 'matara' ),
                'default'  => "ĐỐI TÁC CỦA CHÚNG TÔI"
            ),
            array(
                'id'       => 'matara_partner_content',
                'type'     => 'editor',
                'title'    => __( 'Nội dung đối tác', 'matara' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '<p>
                Trên chặng đường hoạt động và phát triển của Matara Pharma, mỗi đối tác đều là một tài sản quý giá.Chúng tôi vô cùng vinh hạnh là đối tác phân phối độc quyền của nhiều đơn vị y tế trong nước cũng như quốc tế.
                </p>'
            ),
            array(
                'id'          => 'matara_slider_partner',
                'type'        => 'slides',
                'title'       => __('Thêm logo đối tác', 'matara'),
                'placeholder' => array(
                    'title'           => __('Tiêu đê', 'matara'),
                    'description'     => __('Mô tả', 'matara'),
                    'url'             => __('Link', 'matara'),
                ),
            ),

            array(
                'id'       => 'matara_link_facebook',
                'type'     => 'text',
                'title'    => __( 'Link facebook', 'matara' ),
                'default'  => "#"
            ),
            array(
                'id'       => 'matara_link_youtube',
                'type'     => 'text',
                'title'    => __( 'Link youtube', 'matara' ),
                'default'  => "#"
            ),
            array(
                'id'       => 'matara_link_twitter',
                'type'     => 'text',
                'title'    => __( 'Link twitter', 'matara' ),
                'default'  => "#"
            ),

            array(
                'id'       => 'matara_main_footer',
                'type'     => 'editor',
                'title'    => __( 'Main footer', 'matara' ),
                'args'=> array(
                    'teeny' => false,
                    'dfw' => true,
                    'tinymce'=> array(
                        'remove_linebreaks' => false,
                        'wpautop'=>false,
                        'block_formats'=>"Paragraph=p; Heading 3=h3; Heading 4=h4"),
                        'toolbar2' => 'formatselect,underline,alignjustify,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help '
                    ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '<h5>
                CÔNG TY CỔ PHẦN DƯỢC MỸ PHẨM MATARA
            </h5>
            <p>520 Toà nhà HeiTower, Số 1 Nguỵ Như Kon Tum, P. Nhân Chính, Q. Thanh Xuân, Hà Nội</p>
            <p>Hotline: 1900 633 481</p>
            <p>Email: care@matara.vn </p>
            <p>Website: www.matara.vn</p>'
            ),
            array(
                'id'       => 'matara_copyright',
                'type'     => 'text',
                'title'    => __( 'Copyright', 'matara' ),
                'default'  => "Bản quyền ©2020 Matara Pharma., JSC™. Tất cả bản quyền được bảo lưu. Vui lòng liên hệ khi muốn sử dụng nội dung trên trang."
            ),
        )
    ) );


    // -> START Editors
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Home page', 'matara' ),
        'id'               => 'editor',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Slider', 'matara' ),
        'id'         => 'editor-wordpress',
        //'icon'  => 'el el-home'
        'desc'       => __( 'For full documentation on this field, visit: ', 'matara' ) . '<a href="//docs.reduxframework.com/core/fields/editor/" target="_blank">docs.reduxframework.com/core/fields/editor/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'matara_slider',
                'type'     => 'select',
                'title'    => __( 'Chọn slider', 'matara' ),
                'subtitle' => __( 'Cài đặt slider trang chủ', 'matara' ),
                'multi'    => true,
                //Must provide key => value pairs for radio options
                'options'  => matara_select_posttype('cv_slider'),
            ),
        ),
        
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Giới thiệu', 'matara' ),
        'id'         => 'matara_about',
        //'icon'  => 'el el-home'
        'subsection' => true,
        'desc'       => __( 'Setup giới thiệu trang home'),
        'fields'     => array(
            array(
                'id'       => 'matara_about_title',
                'type'     => 'text',
                'title'    => __( 'Tiêu đề', 'matara' ),
                'default'  => "GIỚI THIỆU MATARA PHARMA"
            ),
            array(
                'id'       => 'matara_about_content',
                'type'     => 'editor',
                'title'    => __( 'Nội dung giới thiệu', 'matara' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '<p>
                    Giới thiêu về công ty và triết lý kinh doanh. Ví dụ Matara Pharma là đơn vị nghiên cứu, phát triển song song với chuyển giao kỹ thuật, cung ứng nguyên liệu các loại dược mỹ phẩm chất lượng cao, chủng loại sản phẩm đa dạng phong phú, kể cả những sản phẩm chuyên biệt kháng corticoid. Với đội ngũ chuyên viên, nhân viên có kinh nghiệm và trình độ chuyên môn cao cùng dịch vụ chăm sóc khách hàng chuyên nghiệp, Matara Pharma sẽ mang đến sự hài lòng cho quý khách hàng.
                    </p>'
            ),
            array(
                'id'       => 'matara_about_page',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'posts',
                'args'     => array( 'post_type' =>  array( 'page','post' ), 'numberposts' => -1 ),
                'title'    => __( 'Chọn trang giới thiệu', 'matara' ),
            ),
            array(
                'id'       => 'matara_about_button',
                'type'     => 'text',
                'title'    => __( 'Text button', 'matara' ),
                'default'  => "TÌM HIỂU THÊM VỀ MATARA PHARMA"
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Box', 'matara' ),
        'id'         => 'matara_product_feature',
        //'icon'  => 'el el-home'
        'subsection' => true,
        'desc'       => __( 'Box'),
        'fields'     => array(
            array(
                'id'          => 'matara_product_feature_loop',
                'type'        => 'slides',
                'title'       => __('Add mục nổi bật', 'matara'),
                'placeholder' => array(
                    'title'           => __('Tiêu đê', 'matara'),
                    'description'     => __('Mô tả', 'matara'),
                    'url'             => __('Link', 'matara'),
                ),
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Sản phẩm nổi bật', 'matara' ),
        'id'         => 'matara_product_feature_new',
        //'icon'  => 'el el-home'
        'subsection' => true,
        'desc'       => __( 'Sản phẩm nổi bật'),
        'fields'     => array(
            array(
                'id'       => 'matara_about_feature_title',
                'type'     => 'text',
                'title'    => __( 'Tiêu đề sản phẩm nổi bật', 'matara' ),
                'default'  => "SẢN PHẨM NỔI BẬT"
            ),
            array(
                'id'       => 'matara_about_feature_content',
                'type'     => 'editor',
                'title'    => __( 'Nội dung sản phẩm nổi bật', 'matara' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '<p>
                Sản phẩm nổi bật của Matara Pharma. Ví dụ Matara Pharma là đơn vị nghiên cứu, phát triển song song với chuyển giao kỹ thuật, cung ứng nguyên liệu các loại dược mỹ phẩm chất lượng cao, chủng loại sản phẩm đa dạng phong phú, kể cả những sản phẩm chuyên biệt kháng corticoid. Với đội ngũ chuyên viên, nhân viên có kinh nghiệm và trình độ chuyên môn cao cùng dịch vụ chăm sóc khách hàng chuyên nghiệp, Matara Pharma sẽ mang đến sự hài lòng cho quý khách hàng.
                </p>'
            ),
            array(
                'id'          => 'matara_product_feature_loop_new',
                'type'        => 'slides',
                'title'       => __('Add mục nổi bật', 'matara'),
                'placeholder' => array(
                    'title'           => __('Tiêu đê', 'matara'),
                    'description'     => __('Mô tả', 'matara'),
                    'url'             => __('Link', 'matara'),
                ),
            ),
            array(
                'id'       => 'matara_about_page_timhieu',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'posts',
                'args'     => array( 'post_type' =>  array( 'page','post' ), 'numberposts' => -1 ),
                'title'    => __( 'Tìm hiểu thêm về sản phẩm', 'matara' ),
            ),
            array(
                'id'       => 'matara_about_product_button',
                'type'     => 'text',
                'title'    => __( 'Text button', 'matara' ),
                'default'  => "TÌM HIỂU THÊM VỀ SẢN PHẨM"
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Tin tức mới', 'matara' ),
        'id'         => 'matara_new_post',
        //'icon'  => 'el el-home'
        'subsection' => true,
        'desc'       => __( 'Tin tức mới'),
        'fields'     => array(
            array(
                'id'       => 'matara_new_title_home',
                'type'     => 'text',
                'title'    => __( 'Tiêu đề tin tức mới', 'matara' ),
                'default'  => "TIN TỨC MỚI"
            ),
            array(
                'id'          => 'matara_new_posts',
                'type'     => 'select',
                'multi'    => true,
                'data'     => 'posts',
                'args'     => array( 'post_type' =>  array('post' ), 'numberposts' => -1 ),
                'title'    => __( 'Tin tức mới', 'matara' ),
            ),
        )
    ) );




     // -> START Editors
     Redux::setSection( $opt_name, array(
        'title'            => __( 'Blog', 'matara' ),
        'id'               => 'blog-setting',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner category', 'matara' ),
        'id'         => 'banner-blog',
        //'icon'  => 'el el-home'
        'desc'       => __( 'For full documentation on this field, visit: ', 'matara' ) . '<a href="//docs.reduxframework.com/core/fields/editor/" target="_blank">docs.reduxframework.com/core/fields/editor/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'         => 'mtr_banner_blog',
                'type'       => 'media',
                'title'      => __( 'Upload banner blog', 'matara' ),
                'full_width' => true,
                'default' => array( 'url' => get_template_directory_uri() . '/assets/img/banner-about.png' ),
            ),
        ),
        
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Product', 'matara' ),
        'id'               => 'product-setting',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner category product', 'matara' ),
        'id'         => 'banner-cate_product',
        //'icon'  => 'el el-home'
        'desc'       => __( 'For full documentation on this field, visit: ', 'matara' ) . '<a href="//docs.reduxframework.com/core/fields/editor/" target="_blank">docs.reduxframework.com/core/fields/editor/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'         => 'mtr_banner_category_product',
                'type'       => 'media',
                'title'      => __( 'Upload banner category product', 'matara' ),
                'full_width' => true,
                'default' => array( 'url' => get_template_directory_uri() . '/assets/img/banner-about.png' ),
            ),
        ),
        
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner product', 'matara' ),
        'id'         => 'banner-products',
        //'icon'  => 'el el-home'
        'desc'       => __( 'For full documentation on this field, visit: ', 'matara' ) . '<a href="//docs.reduxframework.com/core/fields/editor/" target="_blank">docs.reduxframework.com/core/fields/editor/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'         => 'mtr_banner_product',
                'type'       => 'media',
                'title'      => __( 'Upload banner product', 'matara' ),
                'full_width' => true,
                'default' => array( 'url' => get_template_directory_uri() . '/assets/img/banner-about.png' ),
            ),
        ),
        
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact product', 'matara' ),
        'id'         => 'contact-products',
        //'icon'  => 'el el-home'
        'subsection' => true,
        'fields'     => array(
            array(
                'id'         => 'mtr_contact_product',
                'type'     => 'text',
                'title'    => __( 'Số điện thoại tổng đài', 'matara' ),
                'default'  => "1900 633 481"
            ),
            array(
                'id'         => 'mtr_contact_text_product',
                'type'     => 'text',
                'title'    => __( 'Nội dung kêu gọi', 'matara' ),
                'default'  => "Bạn đang ghé thăm Kanna bằng di động? Hãy bấm vào số điện thoại để gọi nhanh cho chúng tôi."
            ),
        ),
        
    ) );
     // -> START Editors
     Redux::setSection( $opt_name, array(
        'title'            => __( 'Contact', 'matara' ),
        'id'               => 'contact-setting',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact Page', 'matara' ),
        'id'         => 'contact-setting-sec',
        //'icon'  => 'el el-home'
        'desc'       => __( 'Cài đặt trang liên hệ: ', 'matara' ) . '<a href="//docs.reduxframework.com/core/fields/editor/" target="_blank">docs.reduxframework.com/core/fields/editor/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'         => 'mtr_title_contact',
                'type'       => 'text',
                'title'      => __( 'Tiêu đề', 'matara' ),
                'default' => 'CÔNG TY CỔ PHẦN DƯỢC MỸ PHẨM MATARA',
            ),
            array(
                'id'         => 'mtr_address_contact',
                'type'       => 'text',
                'title'      => __( 'Địa chỉ', 'matara' ),
                'default' => '520 Toà nhà HeiTower, Số 1 Nguỵ Như Kon Tum, P. Nhân Chính, Q. Thanh Xuân, Hà Nội',
            ),
            array(
                'id'         => 'mtr_hotline_contact',
                'type'       => 'text',
                'title'      => __( 'Hotline', 'matara' ),
                'default' => 'Hotline: 1900 633 481',
            ),
            array(
                'id'         => 'mtr_hotline_contact_kd',
                'type'       => 'text',
                'title'      => __( 'Số điện thoại phòng kinh doanh', 'matara' ),
                'default' => 'Phòng kinh doanh: 1900 633 481',
            ),
            array(
                'id'         => 'mtr_email_contact',
                'type'       => 'text',
                'title'      => __( 'Email', 'matara' ),
                'default' => 'Email: care@matara.vn',
            ),
            array(
                'id'         => 'mtr_website_contact',
                'type'       => 'text',
                'title'      => __( 'Website', 'matara' ),
                'default' => 'Website: www.matara.vn',
            ),
            array(
                'id'         => 'mtr_link_map_contact',
                'type'       => 'text',
                'title'      => __( 'Link Google map', 'matara' ),
                'default' => 'https://www.google.com/maps/place/Chung+C%C6%B0+Hei+Tower/@21.0031239,105.803189,17z/data=!4m8!1m2!2m1!1zNTIwIFRvw6AgbmjDoCBIZWlUb3dlciwgU-G7kSAxIE5ndeG7tSBOaMawIEtvbiBUdW0sIFAuIE5ow6JuIENow61uaCwgUS4gVGhhbmggWHXDom4sIEjDoCBO4buZaQ!3m4!1s0x3135acbb783f7ae1:0x412c4f127a5967a7!8m2!3d21.0030742!4d105.8053619?hl=vi-VN',
            ),
            array(
                'id'         => 'mtr_sitekey_captcha',
                'type'       => 'text',
                'title'      => __( 'SiteKey Captcha', 'matara' ),
                'subtitle' =>  __( 'Setting SiteKey Captcha Google : https://www.google.com/recaptcha/admin', 'matara' ),
                'default' => '6LeuetYUAAAAACmHI4qoCThK0Rc3Yz1yECC4b9wz',
            ),
            array(
                'id'         => 'email_admin_address',
                'type'       => 'text',
                'title'      => __( 'Mail nhận', 'matara' ),
                'subtitle' =>  __( 'Mail nhận thư', 'matara' ),
                'default' => 'thoaingo.vnkingnet@gmail.com',
            ),
        ),
        
    ) );


    Redux::setSection( $opt_name, array(
        'icon'            => 'el el-list-alt',
        'title'           => __( 'Customizer Only', 'matara' ),
        'desc'            => __( '<p class="description">This Section should be visible only in Customizer</p>', 'matara' ),
        'customizer_only' => true,
        'fields'          => array(
            array(
                'id'              => 'opt-customizer-only',
                'type'            => 'select',
                'title'           => __( 'Customizer Only Option', 'matara' ),
                'subtitle'        => __( 'The subtitle is NOT visible in customizer', 'matara' ),
                'desc'            => __( 'The field desc is NOT visible in customizer.', 'matara' ),
                'customizer_only' => true,
                //Must provide key => value pairs for select options
                'options'         => array(
                    '1' => 'Opt 1',
                    '2' => 'Opt 2',
                    '3' => 'Opt 3'
                ),
                'default'         => '2'
            ),
        )
    ) );

    if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
        $section = array(
            'icon'   => 'el el-list-alt',
            'title'  => __( 'Documentation', 'matara' ),
            'fields' => array(
                array(
                    'id'       => '17',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                    //'content' => 'Raw content here',
                ),
            ),
        );
        Redux::setSection( $opt_name, $section );
    }
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'matara' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'matara' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

