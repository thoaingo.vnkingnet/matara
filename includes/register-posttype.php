<?php
if(!class_exists('DVC_Template')) {
	class DVC_Template {
		public function __construct() {
			// add_action( 'init', [ $this, '_initCV' ], 10 );
			add_action( 'init', [ $this, '_initSlider' ], 10 );
			add_action( 'init', [ $this, '_initProduct' ], 10 );
		}
		public function _initProduct(){
			$register_faq = apply_filters( 'ccv_apply_register_product', true );
			if ( ! $register_faq ) {
				return false;
			}

			$labels = array(
				'name'               => __( 'Sản phẩm', 'matara' ),
				'singular_name'      => __( 'Sản phẩm', 'matara' ),
				'add_new'            => __( 'Thêm sản phẩm', 'matara' ),
				'add_new_item'       => __( 'Thêm sản phẩm', 'matara' ),
				'edit_item'          => __( 'Sửa sản phẩm', 'matara' ),
				'new_item'           => __( 'Thêm sản phẩm', 'matara' ),
				'view_item'          => __( 'Xem sản phẩm', 'matara' ),
				'search_items'       => __( 'Tìm kiếm sản phẩm', 'matara' ),
				'not_found'          => __( 'Không có sản phẩm nào', 'matara' ),
				'not_found_in_trash' => __( 'Không có sản phẩm nào', 'matara' ),
				'parent_item_colon'  => __( 'Sản phẩm cha:', 'matara' ),
				'menu_name'          => __( 'Sản phẩm', 'matara' ),
			);
			$args   = array(
				'labels'              => $labels,
				'hierarchical'        => true,
				'description'         => 'Danh sách sản phẩm',
				'supports'            => array( 'title', 'editor', 'thumbnail' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-cart',
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'capability_type'     => 'post',
				'rewrite' => array('slug' => 'san-pham', 'with_front' => true)
			);
			register_post_type( 'san-pham', $args );
			$this->_registerAttributeProduct();
		}
		public function _registerAttributeProduct(){
			$name   = __( 'Danh mục sản phẩm', 'matara' );
			$labels = [
				'name'              => $name,
				'singular_name'     => $name,
				'search_items'      => sprintf( __( 'Tìm kiếm %s', 'matara' ), $name ),
				'all_items'         => sprintf( __( 'Tất cả %s', 'matara' ), $name ),
				'parent_item'       => sprintf( __( 'Cha %s', 'matara' ), $name ),
				'parent_item_colon' => sprintf( __( 'Cha %s', 'matara' ), $name ),
				'edit_item'         => sprintf( __( 'Sửa %s', 'matara' ), $name ),
				'update_item'       => sprintf( __( 'Cập nhật %s', 'matara' ), $name ),
				'add_new_item'      => sprintf( __( 'Thêm %s', 'matara' ), $name ),
				'new_item_name'     => sprintf( __( 'Thêm %s', 'matara' ), $name ),
				'menu_name'         => $name,
			];

			$args = [
				'hierarchical' => true,
				'labels'       => $labels,
				'show_ui'      => true,
				'query_var'    => true,
				'show_admin_column' => true,
				'rewrite' => array('slug' => 'danh-muc-san-pham', 'with_front' => true)
				//'update_count_callback' => 'wp_update_term_count_now'
			];
			register_taxonomy('danh-muc-san-pham', 'san-pham', $args);
			
		}


		public function _initSlider(){
			$register_Slider = apply_filters( 'tth_apply_register_Slider', true );
			if ( ! $register_Slider ) {
				return false;
			}

			$labels = array(
				'name'               => __( 'Slider', 'matara' ),
				'singular_name'      => __( 'Slider', 'matara' ),
				'add_new'            => __( 'Add New Slider', 'matara' ),
				'add_new_item'       => __( 'Add New Slider', 'matara' ),
				'edit_item'          => __( 'Edit Slider', 'matara' ),
				'new_item'           => __( 'New Slider', 'matara' ),
				'view_item'          => __( 'View Slider', 'matara' ),
				'search_items'       => __( 'Search Slider', 'matara' ),
				'not_found'          => __( 'No Slider found', 'matara' ),
				'not_found_in_trash' => __( 'No Slider found in Trash', 'matara' ),
				'parent_item_colon'  => __( 'Parent Slider:', 'matara' ),
				'menu_name'          => __( 'Slider', 'matara' ),
			);
			$args   = array(
				'labels'              => $labels,
				'hierarchical'        => true,
				'description'         => 'List Slider',
				'supports'            => array( 'title', 'editor', 'thumbnail' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-image-flip-horizontal',
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'capability_type'     => 'post',
				'rewrite' => array('slug' => 'cv-slider', 'with_front' => false)
			);
			register_post_type( 'cv_slider', $args );
		}

	}
	new DVC_Template();
}
?>